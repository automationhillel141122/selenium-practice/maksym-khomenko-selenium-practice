package org.khomenko;

import org.openqa.selenium.Alert;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.Assert;
import org.testng.annotations.*;


public class AlertTests {

    WebDriver driver;
    SeleniumLib seleniumLib;

    @BeforeClass
    public void beforeClass() {
        driver = new ChromeDriver();
        driver.manage().window().maximize();
        seleniumLib = new SeleniumLib(driver);

    }

    @AfterClass
    public void afterClass() {
        if (driver != null) {
            driver.quit();
        }
    }

    @BeforeMethod
    public void beforeMethod() {
        seleniumLib.goToUrl("https://the-internet.herokuapp.com/javascript_alerts");
    }

    @Test
    public void clickJSAlert() {
        seleniumLib.clickOnKSAlertButton();
        String alertText = seleniumLib.workWithAlertAndCLose(true);
        String resultText = seleniumLib.getResultString();

        Assert.assertEquals(alertText, "I am a JS Alert");
        Assert.assertEquals(resultText, "You successfully clicked an alert");
    }

    @Test
    public void clickJSConfirm() {
        seleniumLib.clickOnJSConfirmButton();
        String alertText = seleniumLib.workWithAlertAndCLose(true);
        String resultText = seleniumLib.getResultString();
        Assert.assertEquals(resultText, "You clicked: Ok");
    }

    @Test
    public void clickJSCancel() {
        seleniumLib.clickOnJSConfirmButton();
        String alertText = seleniumLib.workWithAlertAndCLose(false);
        String resultText = seleniumLib.getResultString();
        Assert.assertEquals(resultText, "You clicked: Cancel");
    }

    @Test
    public void clickJSPromptWriteTextAndOK() {
        seleniumLib.clickOnJSPromptButton();
        Alert WritingText =driver.switchTo().alert();
        String enterText = "It`s only test!";

        SeleniumLib.WorkWithTextAndAccept(WritingText, enterText);

    }

    @Test
    public void clickJSPromptDontWriteTextAndOK() {
        seleniumLib.clickOnJSPromptButton();
        Alert WritingText =driver.switchTo().alert();
        String enterText = "";
        SeleniumLib.WorkWithTextAndAccept(WritingText, enterText);
    }

    @Test
    public void clickJSPromptWriteTextAndCancel(){
        seleniumLib.clickOnJSPromptButton();
        Alert WritingText =driver.switchTo().alert();
        WritingText.sendKeys("It`s only test!");
        WritingText.dismiss();
        String resultText = seleniumLib.getResultString();
        Assert.assertEquals(resultText, "You entered: null");
    }

    @Test
    public void clickJSPromptDontWriteTextAndCancel() {
        seleniumLib.clickOnJSPromptButton();
        Alert WritingText =driver.switchTo().alert();
        WritingText.dismiss();
        String resultText = seleniumLib.getResultString();
        Assert.assertEquals(resultText, "You entered: null");
    }


}
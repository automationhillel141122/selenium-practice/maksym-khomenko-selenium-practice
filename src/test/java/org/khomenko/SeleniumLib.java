package org.khomenko;

import org.openqa.selenium.*;
import org.testng.Assert;

public class SeleniumLib {
    public static WebDriver driver;

    public SeleniumLib(WebDriver driver) {
        this.driver = driver;
    }


    public void goToUrl(String url) {
        driver.get(url);
    }

    public WebElement clickOnJSAlertButton() {
        return clickOnButtonWithText(MyButtons.ALERT.getTextOnButton());
    }

    public WebElement clickOnJSConfirmButton() {
        return clickOnButtonWithText(MyButtons.CONFIRM.getTextOnButton());
    }

    public WebElement clickOnJSPromptButton() {
        return clickOnButtonWithText(MyButtons.PROMPT.getTextOnButton());
    }

    private WebElement clickOnButtonWithText(String textOnButton) {
        WebElement button = driver
                .findElement(By.xpath("//button[text()='%s']".formatted(textOnButton)));
        button.click();
        return button;
    }





    public static String getResultString() {
        WebElement result = driver.findElement(By.id("result"));
        return result.getText();
    }





    /**
     * Switch to alert window, save alert text, enter text to the alert
     * if needed and accept or dismiss it
     *
     * @param accept how to accept alert. If true we accept it and dismiss otherwise
     * @param text   text to be entered in alert
     * @return text from alert
     */
    public String workWithAlertAndCLose(boolean accept, String... text) {
        Alert alert = driver.switchTo().alert();
        String alertText = alert.getText();

        if (text.length > 0) {
            alert.sendKeys(text[0]);
        }

        if (accept) {
            alert.accept();
        } else {
            alert.dismiss();
        }
        return alertText;
    }

    public static void WorkWithTextAndAccept(Alert WritingText, String enterText) {
        WritingText.sendKeys(enterText);
        WritingText.accept();
        String resultText = getResultString();
        if (enterText != "") {
            Assert.assertEquals(resultText, "You entered: " + enterText);
        } else {
            Assert.assertEquals(resultText, "You entered:");
        }
    }

    private void clickOnButtonWithTextJS(MyButtons myButtons) {
        String script = "";
        switch (myButtons){
            case ALERT -> script = "jsAlert();";
            case PROMPT -> script = "jsPrompt();";
            case CONFIRM -> script = "jsConfirm();";
        }
        ((JavascriptExecutor) driver).executeScript(script);
    }

    public  void clickOnJSAlertButtonJS() {
        clickOnButtonWithTextJS(MyButtons.ALERT);
    }

    public  void clickOnJSAlertButtonJS1() {
        clickOnButtonJS(MyButtons.ALERT);
    }

    public static String getResultStringJS() {

        return  ((JavascriptExecutor) driver)
                .executeScript("return arguments[0].textContent",
                        driver.findElement(By.id("result")))
                .toString();
    }

    private void clickOnButtonJS(MyButtons myButtons) {
        JavascriptExecutor javascriptExecutor = (JavascriptExecutor) driver;
        WebElement button = driver
                .findElement(By.xpath("//button[text()='%s']".formatted(myButtons.getTextOnButton())));
        javascriptExecutor.executeScript("arguments[0].click();", button);
    }








}

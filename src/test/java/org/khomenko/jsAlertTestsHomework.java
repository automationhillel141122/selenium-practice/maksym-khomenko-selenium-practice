package org.khomenko;

import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;
import org.testng.Assert;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

public class jsAlertTestsHomework extends BaseTestClass{
    @BeforeMethod
    public void beforeMethod() {
        seleniumLib.goToUrl("https://the-internet.herokuapp.com/javascript_alerts");
    }

    //Test1.1 Click OK for JS Confirm
    @Test
    public void clickForJSConfirmOkJS() {
        JavascriptExecutor javascriptExecutor = (JavascriptExecutor) driver;
        String script = "jsConfirm()";
        javascriptExecutor.executeScript(script);
        Alert alert = driver.switchTo().alert();
        String alertText = alert.getText();
        alert.accept();
        WebElement result =driver.findElement(By.id("result"));
        String resultText = result.getText();
        Assert.assertEquals(alertText, "I am a JS Confirm");
        Assert.assertEquals(resultText, "You clicked: Ok");
    }

    //Test1.2 Click CANCEL for JS Confirm
    @Test
    public void clickForJSConfirmCancelJS() {
        JavascriptExecutor javascriptExecutor = (JavascriptExecutor) driver;
        String script = "jsConfirm()";
        javascriptExecutor.executeScript(script);
        Alert alert = driver.switchTo().alert();
        String alertText = alert.getText();
        alert.dismiss();
        WebElement result =driver.findElement(By.id("result"));
        String resultText = result.getText();
        Assert.assertEquals(alertText, "I am a JS Confirm");
        Assert.assertEquals(resultText, "You clicked: Cancel");
    }

    //Test1.3 Click OK for JS Prompt.
    @Test
    public void clickForJSPromptOkTextJS() {
        JavascriptExecutor javascriptExecutor = (JavascriptExecutor) driver;
        String script = "jsPrompt()";
        javascriptExecutor.executeScript(script);
        Alert alert = driver.switchTo().alert();
        String enterText = alert.getText();
        alert.sendKeys("It`s only test!");
        alert.accept();
        WebElement result =driver.findElement(By.id("result"));
        String resultText = result.getText();
        Assert.assertEquals(enterText, "I am a JS prompt");
        Assert.assertEquals(resultText, "You entered: It`s only test!");
    }

    //Test1.4 Click OK  text - empty for JS Prompt.
    @Test
    public void clickForJSPromptOkTextEmptyJS() {
        JavascriptExecutor javascriptExecutor = (JavascriptExecutor) driver;
        String script = "jsPrompt()";
        javascriptExecutor.executeScript(script);
        Alert alert = driver.switchTo().alert();
        String enterText = alert.getText();
        alert.accept();
        WebElement result =driver.findElement(By.id("result"));
        String resultText = result.getText();
        Assert.assertEquals(enterText, "I am a JS prompt");
        Assert.assertEquals(resultText, "You entered:");
    }

    //Test1.5 Enter text and click CANCEL for JS Prompt.
    @Test
    public void clickForJSPromptCancelTextJS() {
        JavascriptExecutor javascriptExecutor = (JavascriptExecutor) driver;
        String script = "jsPrompt()";
        javascriptExecutor.executeScript(script);
        Alert alert = driver.switchTo().alert();
        String enterText = alert.getText();
        alert.sendKeys("It`s only test");
        alert.dismiss();
        WebElement result =driver.findElement(By.id("result"));
        String resultText = result.getText();
        Assert.assertEquals(enterText, "I am a JS prompt");
        Assert.assertEquals(resultText, "You entered: null");
    }

    //Test1.6 Don`t enter text and click CANCEL for JS Prompt.
    @Test
    public void clickForJSPromptCancelTextEmptyJS() {
        JavascriptExecutor javascriptExecutor = (JavascriptExecutor) driver;
        String script = "jsPrompt()";
        javascriptExecutor.executeScript(script);
        Alert alert = driver.switchTo().alert();
        String enterText = alert.getText();
        alert.dismiss();
        WebElement result =driver.findElement(By.id("result"));
        String resultText = result.getText();
        Assert.assertEquals(enterText, "I am a JS prompt");
        Assert.assertEquals(resultText, "You entered: null");
    }


    //Test2.1  Click OK for JS Confirm
    @Test
    public void clickForJSConfirmOkJS2() {
        WebElement button = driver.findElement(By.xpath("//button[text()='Click for JS Confirm']"));
        JavascriptExecutor javascriptExecutor = (JavascriptExecutor) driver;
        javascriptExecutor.executeScript("arguments[0].click();", button);
        Alert alert = driver.switchTo().alert();
        String enterText = alert.getText();
        alert.accept();
        WebElement result =driver.findElement(By.id("result"));
        String resultText = result.getText();
        Assert.assertEquals(enterText, "I am a JS Confirm");
        Assert.assertEquals(resultText, "You clicked: Ok");
    }

    //Test2.2 Click CANCEL for JS Confirm
    @Test
    public void clickForJSConfirmCancelJS2() {
        WebElement button = driver.findElement(By.xpath("//button[text()='Click for JS Confirm']"));
        JavascriptExecutor javascriptExecutor = (JavascriptExecutor) driver;
        javascriptExecutor.executeScript("arguments[0].click();", button);
        Alert alert = driver.switchTo().alert();
        String enterText = alert.getText();
        alert.dismiss();
        WebElement result =driver.findElement(By.id("result"));
        String resultText = result.getText();
        Assert.assertEquals(enterText, "I am a JS Confirm");
        Assert.assertEquals(resultText, "You clicked: Cancel");
    }

    //Test2.3 Click OK for JS Prompt.
    @Test
    public void clickForJSPromptOkTextJS2() {
        WebElement button = driver.findElement(By.xpath("//button[text()='Click for JS Prompt']"));
        JavascriptExecutor javascriptExecutor = (JavascriptExecutor) driver;
        javascriptExecutor.executeScript("arguments[0].click();", button);
        Alert alert = driver.switchTo().alert();
        String enterText = alert.getText();
        alert.sendKeys("It`s only test");
        alert.accept();
        WebElement result =driver.findElement(By.id("result"));
        String resultText = result.getText();
        Assert.assertEquals(enterText, "I am a JS prompt");
        Assert.assertEquals(resultText, "You entered: It`s only test");
    }

    //Test2.4  //Test1.4 Click OK  text - empty for JS Prompt.
    @Test
    public void clickForJSPromptOkTextEmptyJS2() {
        WebElement button = driver.findElement(By.xpath("//button[text()='Click for JS Prompt']"));
        JavascriptExecutor javascriptExecutor = (JavascriptExecutor) driver;
        javascriptExecutor.executeScript("arguments[0].click();", button);
        Alert alert = driver.switchTo().alert();
        String enterText = alert.getText();
        alert.accept();
        WebElement result =driver.findElement(By.id("result"));
        String resultText = result.getText();
        Assert.assertEquals(enterText, "I am a JS prompt");
        Assert.assertEquals(resultText, "You entered:");
    }

    //Test2.5 Enter text and click CANCEL for JS Prompt.
    @Test
    public void clickForJSPromptCancelTextJS2() {
        WebElement button = driver.findElement(By.xpath("//button[text()='Click for JS Prompt']"));
        JavascriptExecutor javascriptExecutor = (JavascriptExecutor) driver;
        javascriptExecutor.executeScript("arguments[0].click();", button);
        Alert alert = driver.switchTo().alert();
        String enterText = alert.getText();
        alert.sendKeys("It`s only test");
        alert.dismiss();
        WebElement result =driver.findElement(By.id("result"));
        String resultText = result.getText();
        Assert.assertEquals(enterText, "I am a JS prompt");
        Assert.assertEquals(resultText, "You entered: null");
    }

    //Test2.6. Don`t enter text and click CANCEL for JS Prompt.
    @Test
    public void clickForJSPromptCancelTextEmptyJS2() {
        WebElement button = driver.findElement(By.xpath("//button[text()='Click for JS Prompt']"));
        JavascriptExecutor javascriptExecutor = (JavascriptExecutor) driver;
        javascriptExecutor.executeScript("arguments[0].click();", button);
        Alert alert = driver.switchTo().alert();
        String enterText = alert.getText();
        alert.dismiss();
        WebElement result =driver.findElement(By.id("result"));
        String resultText = result.getText();
        Assert.assertEquals(enterText, "I am a JS prompt");
        Assert.assertEquals(resultText, "You entered: null");
    }
}
